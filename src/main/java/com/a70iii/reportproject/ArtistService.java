/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.a70iii.reportproject;

import java.util.List;

/**
 *
 * @author uSeR
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByTotalPrice(){
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(10);
    }
}
